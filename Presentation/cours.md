# markdown cours

### C'est quoi un markdown?  
Markdown est un langage de prise de note ou de Mise en forme d’une web 
### markdown syntaxes 
#### mot en italique
Voici un mot *en italique* 
Votre mot se trouve entre astérisques `*mon-mot*`
-----------------
#### un mot en gras
Voici un mot __en gras__ ! 
Votre mot se trouve entre deux `__underscores__` 
-----------------
#### Les titres
# Titre de niveau 1 
Pour un titre de niveau 1 (h1), il faut placer un `#titre` devant votre titre.
## Titre de niveau 2
Pour un titre de niveau 2 (h2), il faut cette fois deux `##titre` devant votre titre
Et ainsi de suite jusqu'au h6.

#### Aller à la ligne en fin de phrase

Voici un  
Pour faire un  
changement de ligne

Votre ligne doit se terminer par 2 `espaces` pour faire ce qu'on appelle un __retour-chariot__, c'est à dire aller à la ligne.

`Voici un  `
`changement de ligne`

#### Faire une liste à puces
* Une puce
* Une autre puce
* Et encore une autre puce !
Il faut simplement placer un astérisque devant les éléments de votre liste.
`* Une puce`
`* Une autre puce`
`* Et encore une autre puce !`
#### Pour faire une liste ordonnée : 
1. Et de un
2. Et de deux
3. Et de trois  

`1. Et de un`
`2. Et de deux`
`3. Et de trois`
#### Pour imbriquer une liste dans une autre :
* Une puce
* Une autre puce
    * Une sous-puce
    * Une autre sous-puce
* Et encore une autre puce !
`* Une puce`
`* Une autre puce`
    `* Une sous-puce`
    
    `* Une autre sous-puce`
    
`* Et encore une autre puce !`

1. Une puce
2. Une autre puce
    1. Une sous-puce
    2. Une autre sous-puce
3. Et encore une autre puce !
`1. Une puce`
`2. Une autre puce`
    `1. Une sous-puce`
    
    `2. Une autre sous-puce`
    
`3. Et encore une autre puce !`
####Ecrire du code avec des Backticks

Voici un code en javascript
        let nom = sarr;
        console.log(nom)
### Comment stocker son MD avec gitlab


